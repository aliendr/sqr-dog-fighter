package com.example.dog.fighter;

import lombok.Data;

@Data
public class DogDTO {
    Integer id;

    String breed;

    String image;

    Integer rating;

    public DogDTO(Integer id, String breed, String image, Integer rating) {
        this.id = id;
        this.breed = breed;
        this.image = image;
        this.rating = rating;
    }

    public DogDTO(){};
}

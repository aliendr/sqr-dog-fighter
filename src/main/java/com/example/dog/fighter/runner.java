package com.example.dog.fighter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class runner implements ApplicationRunner {

    @Autowired
    DogApiClient dogApiClient;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        System.out.println(dogApiClient.getImages("hound").status);
        System.out.println(dogApiClient.getImages("hound").message);

    }
}
